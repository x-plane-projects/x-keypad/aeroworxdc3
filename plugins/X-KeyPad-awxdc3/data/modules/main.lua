-- main.lua - main file for this script.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         7/28/2020   Dakota Pilot
--                          Original version.
--  1.1         9/1/2020    Dakota Pilot
--                          Added fuel quantity calculations and transponder
--                          custom string.
--  1.2         9/12/2020   Dakota Pilot
--                          Added parameter to initialize call.
--                          Added DME custom string processing.
--  1.3         10/8/2020   Dakota Pilot
--                          Added starter and primer processing and
--                          reload config button.
--  1.4         12/15/2020  Dakota Pilot
--                          Fixed incorrect fuel flow readings.
--                          Added speed values to the Info panel
--=============================================================================
-- Datarefs and commands for all modules

-- These are documented here to keep the information in one place for easy
-- reference instead of having to search all the modules.
-- The indices here are X-Plane zero based.  Use Lua 1 based in the code so add
-- 1 to these values when they are indexed in Lua.

--------------------------------------------------------------------------------
-- Simulator Datarefs

--  Module(s)           Dataref
--  cowl_flaps          sim/cockpit2/engine/actuators/cowl_flap_ratio
--  transponder         sim/cockpit2/radios/actuators/transponder_mode
--  engines             awx/c47/Cockpit/mixture_lever_L
--                      awx/c47/Cockpit/mixture_lever_R
--  fuel                sim/cockpit2/fuel/fuel_quantity
--  information         sim/cockpit2/gauges/indicators/wind_heading_deg_mag
--                      sim/cockpit2/gauges/indicators/wind_speed_kts
--  dme                 sim/cockpit/radios/nav1_dme_dist_m
--                      sim/cockpit/radios/nav2_dme_dist_m
--                      sim/cockpit/switches/DME_radio_selector

--------------------------------------------------------------------------------
-- Simulator Commands

--	Module(s)			Command

--------------------------------------------------------------------------------
-- X-KeyPad datarefs

--	Module(s)			Dataref
--

--------------------------------------------------------------------------------
-- X-KeyPad custom strings used
--
--  Module(s)           String             Usage
--  cowl_flaps              10          Left cowl flap position string
--  cowl_flaps              11          cowl flap position as a string
--  transponder              0          Transponder mode as a string
--  engines                  1          Left mixture setting as a string
--                           2          Right mixture setting as a string
--  fuel                     3          Left Main
--                           4          Right Main
--                           5          Right Aux
--                           6          Left Aux
--  dme                      7          Displays DME selected, nm for both DMEs

--------------------------------------------------------------------------------
-- X-Keypad Shared Ints used.
--
-- 0-3 may be used by fuel management.
--  Module(s)           Int Index       Usage
--  fuel_management          0          Contains left engine X-KeyPad tank
--                                      selection.
--  fuel_management          1          Contains right engine X-Keypad tank
--                                      selection.
--  cowl_flaps              10          Left cowl flap X-KeyPad position
--  cowl_flaps              11          Right cowl flap X-KeyPad position

--------------------------------------------------------------------------------
-- X-Keypad Shared Floats used.

--  Module(s)           Float Index       Usage

--=============================================================================
-- These are datarefs, custom strings, shared ints/floats and commands actually
-- used by main.lua
-- Simulator Datarefs

--------------------------------------------------------------------------------
-- Simulator Commands

--=============================================================================
-- X-KeyPad datarefs and custom strings
-- Datarefs

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables

--=============================================================================
-- Components definition.
components = {
    cowl_flaps {},
    info {},
    fuel {},
    avionics {},
    engines {}
}

--=============================================================================
--Processing functions

--------------------------------------------------------------------------------
-- Update

function update()
    updateAll(components)
end


