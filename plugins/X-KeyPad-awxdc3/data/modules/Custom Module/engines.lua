-- engines.lua - This is the top module for engine functions.
--
-- This module processes engine functions and may use submodules as needed.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         08/31/2020    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:

--=============================================================================
-- Datarefs and commands for all modules 
-- These are documented here to keep the information in one place for easy
-- reference instead of having to search all the modules.
-- The indices here are X-Plane zero based.  Use Lua 1 based in the code so add
-- 1 to these values when they are indexed in Lua.
-- Simulator Datarefs

left_mixture = globalProperty("awx/c47/Cockpit/mixture_lever_L")
right_mixture = globalProperty("awx/c47/Cockpit/mixture_lever_R")

--------------------------------------------------------------------------------
-- Simulator Commands
--=============================================================================
-- X-KeyPad datarefs used

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

custom_string_01 = globalProperty("SRS/X-KeyPad/custom_string_1")
custom_string_02 = globalProperty("SRS/X-KeyPad/custom_string_2")

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--  Module(s)           Int Index       Usage

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--  Module(s)           Float Index       Usage

--=============================================================================
-- Local variables
local mixmode = {"CUT-OFF",
    "AUTO LEAN",
    "AUTO RICH",
    "EMERG"
}

-- These store the last values for the sim and X-Keypad and are used to
-- determine who made the change and update appropriately.
local l_lastsim
local r_lastsim
local l_lastxkeypad
local r_lastxkeypad

--=============================================================================
-- Components

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize() - Synchronize the X-Keypad and simulator variables.
--
-- Parmeters:
--  None
--
-- Returns:
--  None

function initialize(firstcall)

    if firstcall then
        logInfo("Initializing engines.")
    end

    -- Initialize mixture values.
    mixture()

end -- End function initialize

--------------------------------------------------------------------------------
-- mixture - Sets custom string for mixture lever position.
-- Parmeters:
--  None
--
-- Returns:
--  None

function mixture()

    l_lastsim = math.floor(get(left_mixture))
    r_lastsim = math.floor(get(right_mixture))

    -- Set custom strings.
    -- Position, pos, is 0, 1, 2 so convert to 1, 2, or 3.
    set(custom_string_01, mixmode[l_lastsim + 1])
    set(custom_string_02, mixmode[r_lastsim +1])

    -- Set last sim and X-Keypad variables to the sim values
    l_lastxkeypad = l_lastsim
    r_lastxkeypad = r_lastsim

end -- End function mixture
--=============================================================================
--Processing functions
-- Initialization done once on entry.
initialize(true)

----------------------------------------------------------------------------------
-- Update
function update()

    mixture()

end -- End function update
