-- info.lua - Calculations, processing for info panel
--
-- Does miscellaneous processing and calculations for the info and other panels.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         09/06/2020    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:

--=============================================================================
-- Datarefs and commands for all modules 
-- These are documented here to keep the information in one place for easy
-- reference instead of having to search all the modules.
-- The indices here are X-Plane zero based.  Use Lua 1 based in the code so add
-- 1 to these values when they are indexed in Lua.
-- Simulator Datarefs

--  Module(s)           Dataref
--  information         sim/cockpit2/gauges/indicators/wind_heading_deg_mag
--                      sim/cockpit2/gauges/indicators/wind_speed_kts

--------------------------------------------------------------------------------
-- Simulator Commands

--	Module(s)			Commands

--=============================================================================
-- X-KeyPad datarefs used

--	Module(s)			Dataref

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

--  Module(s)           String             Usage

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--  Module(s)           Int Index       Usage

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--  Module(s)           Float Index       Usage

--=============================================================================
-- Local variables

--=============================================================================
-- Components

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize(firstcall) - Synchronize the X-Keypad and simulator variables.
--
-- Parameters:
--  firstcall - true if called on first startup, false if called by a function
--      after startup is done.  Basically used to suppress the startup message.
--
-- Returns:
--  None

function initialize(firstcall)

    if firstcall then
        logInfo("Initializing Info.")
    end

end -- End function initialize

--------------------------------------------------------------------------------
-- wind_calcs - Synchronize the X-Keypad and simulator variables.
--
-- Parameters:
--  None
--
-- Returns:
--  None

function wind_calcs()

end -- End function wind_calcs

--=============================================================================
--Processing functions
-- Initialization done once on entry.

initialize(true)

--------------------------------------------------------------------------------
-- Update
function update()

    wind_calcs()

end -- End function update
