-- cowl_flaps.lua - Controls cowl flap positions.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         8/7/2020   Dakota Pilot
--                          Original version.
--=============================================================================
-- Notes:
--
--  1.  The cowl flaps have three positions - 0 = Closed, 0.5 = Trail, and
--      1= Open but the sim represents them as a float from 0 to 1 to handle
--      aircraft with cowl flaps that have infinite stops.  Since this aircraft
--      only has three positions the values are converted from 0, 0.5, and 1 to
--      0, 1, and 2.
--=============================================================================
-- Simulator datarefs and commands
-- Simulator Datarefs
cowl_flaps_ratio = globalProperty("sim/cockpit2/engine/actuators/cowl_flap_ratio")

--------------------------------------------------------------------------------
-- Simulator Commands

--=============================================================================
-- X-KeyPad datarefs, shared ints, shared floats, and custom strings
-- Datarefs

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings
custom_string_10 = globalProperty("SRS/X-KeyPad/custom_string_10")
custom_string_11 = globalProperty("SRS/X-KeyPad/custom_string_11")

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints
Shared_ints = globalProperty("SRS/X-KeyPad/SharedInt")

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables
-- X-Plane indices into arrays
-- Lua indices into the cowl flaps array (1, based so 1 = XP 0 item)
local xplcowl_index = 1     -- Left cowl flaps
local xprcowl_index = 2     -- Right cowl flaps

-- Shared Ints locations for values
local left_cowlflapindex = 11
local right_cowlflapindex = 12

-- Indicies, positions for custom strings.
local str_leftindex = 10
local str_rightindex = 11
local str_position = {"CLOSED",
                      "TRAIL",
                      "OPEN"
}

-- These store the last values for the sim and X-Keypad and are used to
-- determine who made the change and update appropriately.
local l_lastsim
local r_lastsim
local l_lastxkeypad
local r_lastxkeypad

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize() - syncs X-KeyPad data with the simulator data.
--
-- Parmeters:
--  None
--
-- Returns:
--  None

function initialize(firstcall)

    local left_xkeypadpos
    local right_xkeypadpos
    local pos


    if firstcall then
        logInfo("Initializing cowl flaps")
    end

    -- Sync X-KeyPad cowl flaps settings to the simulator.
    -- Get from X-Plane dataref (Lua based 1 = XP index +1)
    -- The values are converted from 0 to 1 (0, 0.5, 1) to 0,1, 2 for X-KeyPad
    left_xkeypadpos = get_simvalues(xplcowl_index)
    right_xkeypadpos = get_simvalues(xprcowl_index)

    -- Set X-KeyPad Shared Int datarefs
    set(Shared_ints, left_xkeypadpos, left_cowlflapindex)
    set(Shared_ints, right_xkeypadpos, right_cowlflapindex)

    -- Set custom strings.
    -- Position, pos, is 0, 1, 2 so convert to 1, 2, or 3.
    set_strings(left_xkeypadpos + 1, custom_string_10 )
    set_strings(right_xkeypadpos + 1, custom_string_11)

    -- Set last sim and X-Keypad variables to the sim values
    l_lastsim = left_xkeypadpos
    r_lastsim = right_xkeypadpos
    l_lastxkeypad = left_xkeypadpos
    r_lastxkeypad = right_xkeypadpos


end -- End function initialize

--------------------------------------------------------------------------------
-- set_position() - sets the position of the cowl flap handle based on the value of the
-- shared integer. The integer goes from 0 to 2 for 0=closed, 1 = Trail, 2 = Open.
-- These are converted to values between 0 and 1 and written to the dataref.
--
-- Parmeters:
--  None
--
-- Returns:
--  None

function set_position()

    local left_xkeypadpos
    local right_xkeypadpos
    local left_simpos
    local right_simpos

    -- Get the values from the X-KeyPad device shared integer.
    -- This will be 0, 1, or 2.
    left_xkeypadpos = get_xkeypadvalues(left_cowlflapindex)
    right_xkeypadpos = get_xkeypadvalues(right_cowlflapindex)

    -- Get the simulator values which will be float 0 to 1
    left_simpos = get_simvalues(xplcowl_index)
    right_simpos = get_simvalues(xprcowl_index)

    -- See if the sim values have changed and update the X-KeyPad datarefs
    -- Left first
    if (left_simpos ~= l_lastsim) then
        set(Shared_ints, left_simpos, left_cowlflapindex)
        l_lastsim = left_simpos
        l_lastxkeypad = left_simpos
        left_xkeypadpos = get_xkeypadvalues(left_cowlflapindex)
    end
    -- Right next
    if (right_simpos ~= r_lastsim) then
        set(Shared_ints, right_simpos, right_cowlflapindex)
        l_lastsim = right_simpos
        l_lastxkeypad = right_simpos
        right_xkeypadpos = get_xkeypadvalues(right_cowlflapindex)
    end

    -- See if the X-KeyPad values have changed and update the sim datarefs.
    -- The sim values have to range from 0 to 1 so they are divided by 2.0 to
    -- give a float.
    -- Left first.
    if (left_xkeypadpos ~= l_lastxkeypad) then
        set(cowl_flaps_ratio, left_xkeypadpos/2.0, xplcowl_index)
        l_lastxkeypad = left_xkeypadpos
        l_lastsim = left_xkeypadpos
        left_simpos = get_simvalues(xplcowl_index)
    end
    -- Right next
    if (right_xkeypadpos ~= r_lastxkeypad) then
        set(cowl_flaps_ratio, right_xkeypadpos/2.0, xprcowl_index)
        r_lastxkeypad = right_xkeypadpos
        r_lastsim = right_xkeypadpos
        right_simpos = get_simvalues(xprcowl_index)
    end

    ---- Set custom strings.
    ---- Position, pos, is 0, 1, 2 so convert to 1, 2, or 3.
    set_strings(left_xkeypadpos + 1, custom_string_10 )
    set_strings(right_xkeypadpos + 1, custom_string_11)


end --End function set_position

--------------------------------------------------------------------------------
-- set_strings(pos, custstr) - sets custom strings for display.
--
-- Parameters:
--  pos - Shared Int value
--  cststr - the custom string to set.
--
-- Returns:
--  None

function set_strings(pos, custstr)

    set(custstr, str_position[pos])

end -- End function set_strings

--------------------------------------------------------------------------------
-- get_simvalues(index) - Gets the cowl flap value at index from the sim dataref.
-- The values in the sim are floats ranging from 0 to 1 but this aircraft
-- only has three positions - 0, 0.5, 1 so these are converted to 0, 1, and 2
-- to make processing easier.
--
-- Parameters:
--  index - the index in Lua base starting at 1 (XP index + 1) of the value
--      to be retreived from the dataref.
--
-- Returns:
--  The dataref value converted to an integer. 0 -> 0, 0.5 -> 1, 1 -> 2.

function get_simvalues(index)

    return math.floor(get(cowl_flaps_ratio, index) * 2)

end -- End function get_simvalues

--------------------------------------------------------------------------------
-- get_xkeypadvalues(index) - Gets the cowl flap value at index from the
-- X-KeyPad dataref given by index.  These values range can be 0, 1, or 2.
--
-- Parameters:
--  index - the index in Lua base starting at 1 (XP index + 1) of the value
--      to be retreived from the dataref.
--
-- Returns:
--  The value of the X-Keypad dataref at index.

function get_xkeypadvalues(index)

    return get(Shared_ints, index)

end -- End function get_simvalues

--=============================================================================
--Processing functions
-- Initialization done once on entry.
initialize(true)

--------------------------------------------------------------------------------
-- Update
function update()
    -- Set position based on the X-KeyPad or simulator input.
    set_position()

end -- End function update
