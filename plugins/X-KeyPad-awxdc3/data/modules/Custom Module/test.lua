-- test.lua - Test access to XPLM
--
-- Test Access to XPLM.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         09/21/2020    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:

--=============================================================================
-- Simulator Datarefs

--------------------------------------------------------------------------------
-- Simulator Commands

--=============================================================================
-- X-KeyPad datarefs used

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables
local spoken = false


--=============================================================================
-- Components

--=============================================================================
-- Functions
--------------------------------------------------------------------------------
function callback(inIsOk, inRefToOut, inSizeOut, inRefToError, inSizeToError,
                  inOutString, inErrString)
    logInfo("In callback function.")
end -- End function callback

--------------------------------------------------------------------------------
-- initialize(firstcall) - Synchronize the X-Keypad and simulator variables.
--
-- Parameters:
--  firstcall - true if this is being called on startup, false if called by
--  any other routine.
--
-- Returns:
--  None

function initialize(firstcall)

    if firstcall then
        logInfo("Initializing test.")
    end

    --*** Test accessing XPLM functions

    --local ffi = require("ffi")
    --
    ---- Set up for the OS.
    --xplm_lib = {Linux = "Resources/plugins/XPLM_64.so", Windows = "XPLM_64",
    --OSX = "Resources/plugins/XPLM.framework/XPLM"}
    --
    --xplm = ffi.load(xplm_lib[ffi.os])
    --logInfo("Working with OS:" ..xplm_lib[ffi.os])
    --
    --ffi.cdef[[
    --void XPLMGetSystemPath(char *);
    --void XPLMSpeakString(char *);
    --]]
    --
    ----ffi.cdef"void XPLMGetSystemPath(char *);"
    ----local thepath = ffi.string(xplm.XPLMGetSystemPath(ffi.new("char[?]",512)))
    --
    --
    --local c_buffer = ffi.new("char[?]", 512)
    --xplm.XPLMGetSystemPath(c_buffer)
    --thepath = ffi.string(c_buffer)
    --logInfo(thepath)
    --
    --local words = "Cabin temperature is high."
    --c_str = ffi.new("char[?]", #words)
    --ffi.copy(c_str, words)
    --xplm.XPLMSpeakString(c_str)

    --*** Test startprocess
    local pathis = "F:/Programs/notepad++/notepad++.exe"
    local status
    local args = {"test"}
    sasl.startProcessAsync(pathis, { "clist.txt" }, "", true,true, callback)
--    print(status)
    --print(pOut)
    --print(outSize)
    --print(sOut)
    --print(pErr)
    --print(errSize)

end -- End function initialize

--------------------------------------------------------------------------------
function callback(status, pOut, outSize, pErr, errSize, inOutString, inErrString)
    logInfo("In callback function.")
    return
end -- End function callback

--=============================================================================
--Processing functions
-- Initialization done once on entry.
initialize(true)

--------------------------------------------------------------------------------
-- Update
function update()

end -- End function update
