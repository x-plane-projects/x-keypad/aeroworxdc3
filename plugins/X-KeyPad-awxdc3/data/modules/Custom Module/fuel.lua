-- fuel.lua - handles fuel management items on X-KeyPad devices.
-- This module handles tank selection fom X-KeyPad and the simulator.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         8/7/2020   Dakota Pilot
--                          Original version.
--  1.1         9/1/2020    Dakota Pilot
--                          Added fuel quantity calculations

--=============================================================================
-- Notes:
--
--  This script process the fuel systems on the aircraft.
--      1.0     On startup the X-KeyPad tank selector positions for left and
--              tanks are synchronized with the simulator values.

--=============================================================================
-- Simulator Datarefs
-- Tank selector position
--  0 = Off
--  1 = Left Aux
--  2 = Left Main
--  3 = Right Main
--  4 = Right Aux
current_tank_left = globalProperty("awx/c47/cockpit/fuel_tank_selector_left")
current_tank_right = globalProperty("awx/c47/cockpit/fuel_tank_selector_right")
fuel_quant_kgs = globalProperty("sim/cockpit2/fuel/fuel_quantity")

--------------------------------------------------------------------------------
-- Simulator Commands
left_selector_down = sasl.findCommand("awx/c47/fuel_left_selector_dwn")
left_selector_up = sasl.findCommand("awx/c47/fuel_left_selector_up")
right_selector_down = sasl.findCommand("awx/c47/fuel_right_selector_dwn")
right_selector_up = sasl.findCommand("awx/c47/fuel_right_selector_up")

--=============================================================================
-- X-KeyPad datarefs and custom strings
-- Datarefs

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings
custom_string_03 = globalProperty("SRS/X-KeyPad/custom_string_3")
custom_string_04 = globalProperty("SRS/X-KeyPad/custom_string_4")
custom_string_05 = globalProperty("SRS/X-KeyPad/custom_string_5")
custom_string_06 = globalProperty("SRS/X-KeyPad/custom_string_6")
--  fuel                     3          Left Main
--                           4          Right Main
--                           5          Right Aux
--                           6          Left Aux

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints
-- This Shared Int indicates which engine and which tank for that engine
-- is being processed.  Set the function tank_selection for more details.
l_xkeypad_tankselection = globalProperty("SRS/X-KeyPad/SharedInt[0]")
r_xkeypad_tankselection = globalProperty("SRS/X-KeyPad/SharedInt[1]")

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables
-- Conversion factors multiply item by these to get result (i.e. kg to lbs
-- would be kg value * kgtolb).
local kgtolb = 2.2046226218
local lbtokg = 0.45359237
local lbtogalavgas = 6.02

-- These store the last values for the sim and X-Keypad and are used to
-- determine who made the change and update appropriately.
local l_lastsim
local r_lastsim
local l_lastxkeypad
local r_lastxkeypad

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize() - Synchronize the X-Keypad and simulator variables.

function initialize(firstcall)

    if firstcall then
        logInfo("Initializing fuel.")
    end

    -- Tank selection values
    -- Save current values for later use.
    l_lastsim = get(current_tank_left)
    r_lastsim = get(current_tank_right)
    l_lastxkeypad = l_lastsim
    r_lastxkeypad = r_lastsim

    -- Match X-Keypad to the sim
    set(l_xkeypad_tankselection, l_lastsim)
    set(r_xkeypad_tankselection, r_lastsim)

    -- Synch X-KeyPad fuel quantities to the simulator.
    fuel_quantity()

end -- End function initialize

--------------------------------------------------------------------------------
-- tank_selection - Handles selection of tanks from X-KeyPad
--
-- Tank changes made in the sim are reflected in X-KeyPad because the tank
-- selection dataref is used by X-KeyPad to set the colors to indicate the
-- status of the tank.
--
-- Changes made in X-KeyPad are transferred to the simulator using a Shared_Int
-- dataref that is set to a value indicating what tank was requested.
-- Shared_Int[0] is used for the left tank and Shared_Int[1] for the right tank.
-- The Shared_Ints take values from 0-4 to indicate the tank selected.
-- When an X-KeyPad key for a tank is pressed the Shared_Int is set to
-- the value representing that tank.
--
-- This function determines which engine is being operated on and what tank
-- is being requested.  It then determines what tank is currently selected
-- and which direction the tank selector handle must move (down or up) and then
-- moves the selector handle the desired number of positions.
--
-- The shared int will contain a number from 0 to 4 to indicate which tank
-- has been selected.
--
--      1 = Left Aux
--      2 = Left Main
--      3 = Right Main
--      4 = Right Aux
--
-- Parameters:
--  None
--
-- Returns:
--  None

function tank_selection()

    local left_xkeypadpos
    local right_xkeypadpos
    local left_simpos
    local right_simpos
    local tankdata
    local currenttank

    -- Did the left tank selection change?
    -- If so change the sim tank selection and save new value as last value.
    tankdata = get(l_xkeypad_tankselection)
    if tankdata ~= l_lastxkeypad then
        currenttank = get(current_tank_left)
        change_tank(tankdata, currenttank, left_selector_up, left_selector_down, current_tank_left)
        l_lastxkeypad = tankdata
        l_lastsim = get(current_tank_left)
    end

    -- Did the right tank selection change?
    -- If so change the sim tank selection and save new value as last value.
    tankdata = get(r_xkeypad_tankselection)
    if tankdata ~= r_lastxkeypad then
        currenttank = get(current_tank_right)
        change_tank(tankdata, currenttank, right_selector_up, right_selector_down, current_tank_right)
        r_lastxkeypad = tankdata
        r_lastsim = get(current_tank_right)
    end

end -- end function tank_selection

--------------------------------------------------------------------------------
-- change_tank(newtank, oldtank, selectorup, selectordown, currenttank) -
-- Changes the simulator fuel tank selection based on the X-KeyPad change.
--
-- The awx command dwn turns the selector left while up turns it right where
-- left is toward the pilot and right toward the copilot.
-- Parameters:
--  newtank - the tank we are to switch to.
--  oldtank - the tank we are switching from.
--  selectorup - the property containing the up selector for this engine.
--  selectordown - the property containing the down selector for this engine.
--  currenttank - the tank currently in use.
--
-- Returns:
--  None

function change_tank(newtank, oldtank, tankselectorup, tankselectordown, currenttank)

    local diff
    local i

    -- Find which way to go - up or down.
    diff = newtank - oldtank

    -- Move the selector the desired number of positions.
    -- Move it, check the value and if it's the desired position quit
    -- otherwise move again.
    -- Move it down (left toward pilot)
    if diff < 0 then
        for i = oldtank, newtank + 1, -1
        do
            sasl.commandOnce(tankselectordown)
        end

    end
    -- Move it up (right toward first officer)
    if diff > 0 then
        for i = oldtank, newtank - 1, 1
        do
            sasl.commandOnce(tankselectorup)
        end
    end
end -- End function change_tank

--------------------------------------------------------------------------------
-- fuel_quantity() - Calculates fuel amounts for each tank and puts the
-- results in a custom string for use by X-Keypad for display purposes..
--
-- Parameters:
--  None
--
-- Returns:
--  None

function fuel_quantity()

    local str_tmp
    local num_lbs
    local num_gals
    local tank_index = {1, 2, 3, 4}
    local tank_custstring = {custom_string_03,
         custom_string_04,
         custom_string_05,
         custom_string_06
    }
    local tank_name = {"L MAIN",
        "R MAIN",
        "R AUX",
        "L AUX"
    }

    -- convert fuel mount from kgs to lbs and gallons for display.
    -- X-Plane dataref indices are
    --  0 - Left Main
    --  1 - Right Main
    --  2 - Right Aux
    --  3 - Left Aux
    -- Lua index from 1, X-Plane from 0.
    for i = 1, 4, 1 do
        num_lbs = get(fuel_quant_kgs, i) * kgtolb
        num_gals = num_lbs/lbtogalavgas
        str_tmp = string.format("%s\nLBS/GAL\n%.1f\n%.1f", tank_name[i], num_lbs, num_gals)
        set(tank_custstring[i], str_tmp)
    end

end -- End function fuel_misc
--=============================================================================
--Processing functions
-- Initialization done once on entry.
initialize(true)

--------------------------------------------------------------------------------
-- Update
function update()

    tank_selection()
    fuel_quantity()

end -- End function update
