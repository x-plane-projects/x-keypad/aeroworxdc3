-- dme.lua - Processes DME functions
--
-- Handles DME processing.  Creates a custom string to indicate which NAV
-- radio is selected for the DME signal and shows distance for each DME (it
-- is assume there are at most two DMEs). This routine is needed because
-- all the information to be displayed can not be done with the csv file.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         09/12/2020    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:

--=============================================================================
-- Simulator Datarefs
dme1_nm = globalProperty("sim/cockpit/radios/nav1_dme_dist_m")
dme2_nm = globalProperty("sim/cockpit/radios/nav2_dme_dist_m")
dme_navsel = globalProperty("sim/cockpit/switches/DME_radio_selector")

--------------------------------------------------------------------------------
-- Simulator Commands

--=============================================================================
-- X-KeyPad datarefs used

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings
custom_string_07 = globalProperty("SRS/X-KeyPad/custom_string_7")

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables

--=============================================================================
-- Components

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize(firstcall) - Synchronize the X-Keypad and simulator variables.
--
-- Parameters:
--  firstcall - true if this is being called on startup, false if called by
--  any other routine.
--
-- Returns:
--  None

function initialize(firstcall)

    if firstcall then
        logInfo("Initializing avionics/dme")
    end

    -- Set X-KeyPad to simulator values
    dme_string()

end -- End function initialize

--------------------------------------------------------------------------------
-- dme_string() - fills out the custom string with the DME informaton.
--
-- Parameters:
--  None
--
-- Returns:
--  None

function dme_string()

    local dme_selected
    local dme1nm
    local dme2nm
    local dme_str
    local dme1_ind
    local dme2_ind

    -- Get DME data
    dme_selected = get(dme_navsel)
    dme1nm = get(dme1_nm)
    dme2nm = get(dme2_nm)

    -- Create the custom string
    if dme_selected ==1 then
        dme1_ind = ">"
        dme2_ind = " "
    else
        dme2_ind = ">"
        dme1_ind = " "
    end
    dme_str = string.format("%s%.1f\nDME %d\n %s%.1f", dme1_ind, dme1nm, dme_selected, dme2_ind, dme2nm)
    set(custom_string_07, dme_str)

end -- End function dme_string

--=============================================================================
--Processing functions
-- Initialization done once on entry.

initialize(true)

--------------------------------------------------------------------------------
-- Update
function update()

    dme_string()
end -- End function update
